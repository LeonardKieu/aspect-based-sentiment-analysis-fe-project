import logo from './logo.svg';
import './ABSA.css';
import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';

class ABSA extends React.Component {
    constructor(props) {
        super(props)
        console.log("Construct!")
        this.state = {
            paragraph: "Enter the review paragraph...",
            resultTable: (
                <tbody>
                    {/* <tr >
                        <td>luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha luluHaha lula luluHaha luluHaha luluHaha lulu</td>
                        <td>4</td>
                        <td>AI</td>
                    </tr>
                    <tr >
                        <td>Haha lulu</td>
                        <td>4</td>
                        <td>AI</td>
                    </tr>
                    <tr >
                        <td>Haha lulu</td>
                        <td>4</td>
                        <td>AI</td>
                    </tr> */}
                </tbody>
            )
        }
    }
    mySubmitHandler = (event) => {
        event.preventDefault();
        console.log("Trigger event!");
        // const apiUrl = 'http://localhost:8000/absa/absa';
        const apiUrl = 'http://ec2-13-229-213-183.ap-southeast-1.compute.amazonaws.com';
        let text = document.getElementById("paragraph").value
        var x = fetch(apiUrl, {
            "method": "POST",
            "headers": {
                "content-type": "application/json",
                "accept": "application/json"
            },
            "body": JSON.stringify({
                paragraph: text
            })
        }).then(res => res.json())
            .then(res => {
                console.log("This is parsed json:")
                console.log(res)
                let list_results = res.returned_results;
                var rows = [];
                for (var i = 0; i < list_results.length; i++) {
                    rows.push(<tr >
                        <td>{list_results[i].text}</td>
                        <td>{list_results[i].tag_name} - {list_results[i].confidence}</td>
                        <td>{list_results[i].topic}</td>
                    </tr>);
                }
                let new_table_body = <tbody>{rows}</tbody>;
                console.log("After loop")
                console.log(new_table_body)
                this.setState({ resultTable: new_table_body });
                console.log("This is new table body")
                console.log(new_table_body)
            })

    }

    render() {
        return (
            <div className="ABSA">
                <form onSubmit={this.mySubmitHandler}>
                    <div className="form-group">
                        <p id="titleParagraph">Review paragraph</p>
                        <textarea className="form-control" id="paragraph" rows="10"></textarea>
                    </div>
                    <input type="submit" id="submitbtn" className="btn btn-default" value="Analyze" onSubmit={this.mySubmitHandler}></input>
                    <table className="table-hover table-striped table-bordered active" id="resultTable">
                        <thead>
                            <tr>
                                <th className="col-md-7">Opinion</th>
                                <th>Sentiment - Confidence</th>
                                <th>Topic keyword</th>
                            </tr>
                        </thead>
                        {this.state.resultTable}
                    </table>
                </form>
            </div>
        );
    }
}

export default ABSA;
